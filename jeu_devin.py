import random
iziefizuehfiuzeifhziuefhizuef


def jeu_devin():
    '''Faire deviner le nombre choisi par la machine entre 0 et 1000'''

    # Faire choisir nombre à la machine
    nombre = random.randint(1, 999)

    # Choisir n
    print("J'ai choisi un nombre compris entre 1 et 1000.")

    # Comptabiliser le nombre d'essai
    n_essai = 1  # Initialiser la variable n_essai
    n = int(input(f"Proposition {n_essai} :"))

    # Déterminer où en est l'utilisateur tant que n est différent de nombre
    while n != nombre:
        # proposer des indices pour chaque essai
        if n > nombre:
            print("Trop grand")  # retourner indice pour aider l'utilisateur
            n_essai += 1  # incrémenter le n_essai à chaque n != nombre
            n = int(input(f"Proposition {n_essai} :"))
        else:
            print("Trop petit")
            n_essai += 1
            n = int(input(f"Proposition {n_essai} :"))

    print("YEEEEEEAAAAAAAHHHHH", n_essai, "essais")
    print("Bravo!!! Vous avez réussi après essais")


jeu_devin()
